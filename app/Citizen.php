<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Citizen extends Model
{
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'identification_card',
        'email',
        'department',
        'city',
        'commune',
        'location',
        'address',
        'phone',
        'rol_id',
        'parent',

        'department_vp',
        'city_vp',
        'voting_post',
        'address_vp',
        'table',
        'zone_vp',
        'commune_vp',

        'todos',
        'gobernacion',
        'alcaldia',
        'concejo',
        'asamblea',
    ];

    /**
     * Get the rol.
     */
    public function rol()
    {
        return $this->belongsTo('App\Rol', 'rol_id', 'id');
    }

    public function children(){
        return $this->hasMany('App\Citizen', 'parent', 'id')->orderBy('rol_id', 'desc');
    }

    public function parent(){
        return $this->hasOne('App\Citizen', 'id', 'parent');
    }
}
