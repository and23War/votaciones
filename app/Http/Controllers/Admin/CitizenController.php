<?php

namespace App\Http\Controllers\Admin;

use App\Citizen;
use App\Commune;
use App\Rol;
use App\Http\Controllers\Controller;
use App\Http\Requests\Citizen\CitizenPost;
use App\Http\Requests\Citizen\CitizenPut;

class CitizenController extends Controller
{
    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->rol_id === 1) {

            $citizens = Citizen::orderBy('id', 'asc')->paginate(50);
        } else {

            $citizens = Citizen::where('parent', \Auth::user()->citizen_id)
                ->orWhere('id', \Auth::user()->citizen_id)->orderBy('id', 'asc')->paginate();
        }
        return view('admin.citizen.index', compact('citizens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Auth::user()->rol_id === 1) {
            $rolsSql = Rol::find([\Auth::user()->rol_id + 1]);
        } else {
            $rolsSql = Rol::find([\Auth::user()->rol_id + 1, 5]);
        }
        $rols = [];
        foreach ($rolsSql as $rol) {
            $rols[$rol->id] = $rol->name;
        }
        return view('admin.citizen.create', compact('rols'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CitizenPost $request)
    {
        $validated = $request->validated();

        if (
            $request->get('todos') ||
            $request->get('gobernacion') ||
            $request->get('alcaldia') ||
            $request->get('concejo') ||
            $request->get('asamblea')
        ) {
            $idc = $validated['identification_card'];
            $response = $this->getRegistraduriaInfo($idc);
            $re = '/<td><b>(.*)<\/b><\/td>|<td>(.*)<\/td>/m';

            if (preg_match_all($re, $response->data->message, $matches, PREG_SET_ORDER, 0)) {
                if (\Auth::user()->rol_id === 1) {
                    $parent = null;
                } else {
                    $parent = \Auth::user()->citizen_id;
                }
                $department_vp = $matches[1][2];
                $city_vp = $matches[2][2];
                $voting_post = $matches[3][2];
                $address_vp = $matches[4][2];
                $table = $matches[5][1];
                $citizen = Citizen::create([

                    'identification_card' => $validated['identification_card'],
                    'rol_id' => $validated['rol_id'],
                    'department_vp' => $department_vp,
                    'city_vp' => $city_vp,
                    'voting_post' => $voting_post,
                    'address_vp' => $address_vp,
                    'table' => $table,
                    'parent' => $parent,

                    'todos' => $request->get('todos'),
                    'gobernacion' => $request->get('gobernacion'),
                    'alcaldia' => $request->get('alcaldia'),
                    'concejo' => $request->get('concejo'),
                    'asamblea' => $request->get('asamblea'),

                ]);
                $message = 'Referido Creado Correctamente';
            } else {
                $message = strip_tags($response->data->message);
                return redirect()->back()->withErrors([$message]);
            }

            return redirect()->route('citizen.edit', ['id' => $citizen->id])->with('info', $message);
        } else {


            $message = "Error Por favor seleccione una opcion entre Concejo, Alcaldia, Gobernacion o Todos";
            return  redirect()->route('citizen.create')->with('message_error', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Citizen  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Citizen $citizen)
    {
        $this->authorize('view', $citizen);
        return view('admin.citizen.show', compact('citizen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function edit(Citizen $citizen)
    {
        $this->authorize('update', $citizen);
        if (\Auth::user()->rol_id === 1) {
            $rolsSql = Rol::find([\Auth::user()->rol_id + 1]);
        } else {
            $rolsSql = Rol::find([\Auth::user()->rol_id + 1, 5]);
        }

        $rols = [];
        foreach ($rolsSql as $rol) {
            $rols[$rol->id] = $rol->name;
        }

        $communeSql = Commune::orderBy('id', 'asc')->get();

        $commune = [];
        foreach ($communeSql as $val) {
            $commune[$val->id] = $val->name;
        }




        return view('admin.citizen.edit', compact('citizen', 'rols', 'commune'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function update(CitizenPut $request, Citizen $citizen)
    {
        $this->authorize('update', $citizen);
        $citizen->fill($request->all());
        $updated = $citizen->save();
        $message = $updated ? 'Referido Actualizado Correctamente' : 'Error al actualizar';
        return redirect()->route('citizen.index')->with('info', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Citizen  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citizen $citizen)
    {
        $this->authorize('delete', $citizen);
        Citizen::destroy($citizen->id);
        $message = 'Referido Eliminado Correctamente';
        return redirect()->route('citizen.index')->with('info', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  String $identification_card
     * @return \Illuminate\Http\Response
     */
    public function getRegistraduriaInfo(String $identification_card)
    {
        $response = new \stdClass();
        $response->data = new \stdClass();
        $response->success = false;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://wsp.registraduria.gov.co/censo/consultar",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "nuip=" . $identification_card,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $responseCURL = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err || $responseCURL === NULL || $responseCURL === "The service is unavailable.") {
            $response->data->message = 'No se pudo consultar la Registraduria';
        } else {
            $response = json_decode($responseCURL);
        }

        return $response;
    }


    public function getimport()
    { }
}
