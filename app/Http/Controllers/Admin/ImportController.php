<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Import;
use App\Comunas;
use App\Department;
use App\City;

class ImportController extends Controller
{
   

    public function import(Request $request)
    {	  	
    	$true = 1;

    	

    	Excel::load($request->file, function($reader) {
      				
		foreach ($reader->get() as $cedula) {
			     
			     Import::create([
			     
			     'cedula' => $cedula->cedula,
			    
			     ]);

			    }
			 
			 });
			 
			 return 'OK';
    
	

	}


	public function duplicate (){

		$duplicate = Import::whereColumn([
                    ['cedula', '=', 'cedula'],
                ])->get();
	}


	public function comunas(Request $request)
    {	  	
    	$true = 1;

    	
    	try{
    	
    	Excel::load($request->file, function($reader) {
      				
		foreach ($reader->get() as $data) {
			     
			$comunas= Comunas::create([
			     
			     'comuna' => $data->comuna,
			     'barrio' => $data->barrio,
			    
			     ]);
				// dd($data);
			    }
			 
			 });
			 
		$message = 'Importacion Creada Correctamente';

			return view('admin.tablasmaestras.index')->with('info', $message);

		}catch (Exception $e) 
		{
		 $message = $e->getMessage()."\n";
			
			return view('admin.tablasmaestras.index')->with('info', $message);
		}
    
	

	}

	public function departamento(Request $request)
    {	  	
    	$true = 1;

    	
    	try{
    	
    	Excel::load($request->file, function($reader) {
      				
		foreach ($reader->get() as $data) {
			     
			$departamento = Department::create([
			     
			     'id' 	=> $data->id,
			     'codigo' => $data->codigo,
			     'nombre' => $data->nombre,
			    
			     ]);
				 // dd($data);
			    }
			 
			 });
			 
		$message = 'Importacion Creada Correctamente';

			return view('admin.tablasmaestras.index')->with('info', $message);

		}catch (Exception $e) 
		{
		 $errors = $e->getMessage()."\n";
			
			return view('admin.tablasmaestras.index')->with('info', $errors);
		}
    
	

	}


	public function ciudad(Request $request)
    {	  	
    	$true = 1;

    	
    	try{
    	
    	Excel::load($request->file, function($reader) {
      				
		foreach ($reader->get() as $data) {
			     
			$city = City::create([
			     
			     'department_id' 	=> $data->department_id,
			     'code' 			=> $data->code,
			     'name' 			=> $data->name,
			     'type' 			=> $data->type,
			    
			     ]);
				
				//dd($data);
			    }
			 
			 });
			 
		$message = 'Importacion Creada Correctamente';

			return view('admin.tablasmaestras.index')->with('info', $message);

		}catch (Exception $e) 
		{
		 $errors = $e->getMessage()."\n";
			
			return view('admin.tablasmaestras.index')->with('info', $errors);
		}
    
	

	}
}
