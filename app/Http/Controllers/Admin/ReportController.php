<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Citizen;
use App\Rol;

class ReportController extends Controller
{
    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $citizens = Citizen::where('rol_id', 2)->orderBy('id', 'asc')->paginate(50);
        return view('admin.reportes.index', compact('citizens'));
    }


    public function reporte1(Request $request)
    {
        //CEDULAS
        $dato = $request->get('dato');

        if ($dato == 1) {
            $citizens = Citizen::where('concejo', 1)->get();
        }

        if ($dato == 2) {
            $citizens = Citizen::where('alcaldia', 1)->get();
        }

        if ($dato == 3) {
            $citizens = Citizen::where('gobernacion', 1)->get();
        }

        if ($dato == 4) {
            $citizens = Citizen::where('todos', 1)->get();
        }

        if ($dato == null) {
            $message = 'Por favor seleccione una opcion';
            return view('admin.reportes.index')->with('info', $message);
        }

        Excel::create("reporte", function ($excel) use ($citizens) {
            $excel->setTitle("Title");
            $excel->sheet("Libro 1", function ($sheet) use ($citizens) {
                $sheet->loadView('admin.reportes.reporte1')->with('citizens', $citizens);
            });
        })->download('xls');
        return back();
    }


    public function ajaxreport(Request $request)
    {
        dd("aja");
        //CEDULAS
        $dato = $request->get('query');
        if ($dato == 1) {
            $query = Citizen::where('concejo', 1)->get();
            return response()->json(
                $query->toArray()
            );
        }

        if ($dato == 2) {
            $query = Citizen::where('alcaldia', 1)->get();
            return response()->json(
                $query->toArray()
            );
        }

        if ($dato == 3) {
            $query = Citizen::where('gobernacion', 1)->get();
            return response()->json(
                $query->toArray()
            );
        }

        if ($dato == 4) {
            $query = Citizen::where('todos', 1)->get();
            return response()->json(
                $query->toArray()
            );
        }

        if ($dato == null) {
            $message = 'Por favor seleccione una opcion';
            return view('admin.reportes.index')->with('info', $message);
        }
    }
}
