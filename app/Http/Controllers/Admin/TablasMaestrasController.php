<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Import;
use App\prueba;
use App\Citizen;
use App\saveresponse;
use App\Department;
use App\City;
use App\Comunas;

class TablasMaestrasController extends Controller
{


    public function index(){

    	return view('admin.tablasmaestras.index');
    }

    public function uploadcomunas(){

        return view('admin.tablasmaestras.uploadcomunas');
    }

    public function uploadcity(){

        return view('admin.tablasmaestras.uploadcity');
    }


    public function uploaddept(){

        return view('admin.tablasmaestras.uploaddept');
    }



     public function upload(){

      return view('admin.tablasmaestras.upload');
    }



    public function exploit(Request $request){

      $exploit = $request->get('run');
    	
      if($exploit == 1){

        $cedulas = Import::whereBetween('id', [3266, 3275])
              ->get();

          //Crear Array con Cedulas
           foreach ($cedulas as $key => $value) {           
            $datos[]=['cedula' => $value->cedula,];
            }

          foreach ($datos as $key => $value) {        
            foreach ($value as $k => $v) {
              
              sleep(5);
            $this->saveinfo($v);
          }
        }
         

      return response()->json( 'exito !!' );

      }// End IF
     
    }

    public function saveinfo($data){

                          

       $response = $this->gdata($data);
      
      $re = '/<td><b>(.*)<\/b><\/td>|<td>(.*)<\/td>/m';
       
        if(preg_match_all($re ,$response->data->message, $matches, PREG_SET_ORDER, 0))
        {
           
            $department_vp = $matches[1][2];
            $city_vp = $matches[2][2];
            $voting_post = $matches[3][2];
            $address_vp = $matches[4][2];
            $table = $matches[5][1];
            

            $citizen = Citizen::create([
                'identification_card' => $data,
                'rol_id' => 5,
                'department_vp' => $department_vp,
                'city_vp' => $city_vp,
                'voting_post' => $voting_post,
                'address_vp' => $address_vp,
                'table' => $table,
                
            ]);

            $message = 'Usuario Creado Correctamente';
        }else{

                 $message = strip_tags($response->data->message);
                   
                    saveresponse::create([

                            'response' => $message,
                    ]);

           
            return $message;
        }

        return  $message;
           
    }

    public function gdata(String $data){

        $response = new \stdClass();
        $response->data = new \stdClass();
        $response->success = false;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://wsp.registraduria.gov.co/censo/consultar",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "nuip=".$data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $responseCURL = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err || $responseCURL === NULL || $responseCURL === "The service is unavailable.") {
            $response->data->message = 'No se pudo consultar la Registraduria';
        } else {
            $response = json_decode($responseCURL);
        }

        return $response;

           
    }


     public function Departament(Request $request){

            $dato  = $request->get('query');

             $query = Department::where('name','LIKE','%'.$dato.'%')            
                         ->get();

            return response()->json(
                       
                     $query->toArray()

                    );

            // return $query;

     }


     public function city(Request $request){

            $dato  = $request->get('query');
            $dato2  = $request->get('id_de');

           


             $query = City::where('department_id', $dato2)
                          ->where('name','LIKE','%'.$dato.'%')            
                         ->get();

            return response()->json(
                      
                    $query->toArray()
                     
                    );

            // return $query;

     }



     public function location(Request $request){

            $dato  = $request->get('query');
            $dato2  = $request->get('comuna');

             $query = Comunas::where('comuna', $dato2)
                          ->where('name','LIKE','%'.$dato.'%')            
                         ->get();

            return response()->json(
                       
                     $query->toArray()

                    );

            // return $query;

     }
     
}