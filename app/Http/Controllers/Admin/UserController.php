<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Citizen;
use App\Rol;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserPost;
use App\Http\Requests\User\UserPut;
use App\Http\Requests\User\UserPutPassword;


class UserController extends Controller
{
    /**
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->rol_id === 1){
            $users = User::orderBy('id', 'asc')->paginate(3);
        }else{
            $users = User::where('parent', \Auth::user()->id)->orWhere('id', \Auth::user()->id)->orderBy('id', 'asc')->paginate();
        }
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\User  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\User\UserPost $userPost
     * @return \Illuminate\Http\Response
     */
    public function store(UserPost $userPost)
    {
        $userPost->validated();

        $user = User::create([
            'name' => $userPost['name'],
            'email' => $userPost['email'],
            'password' => \Hash::make($userPost['password']),
            'rol_id' => $userPost['rol_id'],
            'state_id' => $userPost['state_id'],
            'citizen_id' => $userPost['citizen_id'],
            'parent' => \Auth::user()->id
        ]);
        if($user){
            $message = 'Ususario creado con éxito';
        }
        return redirect()->route('user.index')->with('info', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

     public function show(User $user)
    {
        $this->authorize('view', $user);
        

      
        return view('admin.user.show', compact('user'));
    }


    public function edit(User $user)
    {
        $this->authorize('update', $user);
        $rols = [];
        $states = [];

        $rol = Rol::find($user->rol_id);
        $statesSql = State::all();

        $rols[$rol->id] = $rol->name;
        foreach($statesSql as $state){
            $states[$state->id] = $state->name;
        }

        return view('admin.user.edit', compact(['user', 'rols', 'states']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\User\UserPut  $userPut
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserPut $userPut, User $user)
    {
        $this->authorize('update', $user);
        $userPut->validated();

        $user->fill($userPut->all());
        $updated = $user->save();
        $message = $updated ? 'Usuario Actualizado Correctamente' : 'Error al actualizar';
        return redirect()->route('user.index')->with('info', $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\User\UserPutPassword  $userPut
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(UserPutPassword $userPutPassword, User $user)
    {
        $this->authorize('update', $user);
        $userPutPassword->validated();

        $user = $user::findOrFail($userPutPassword->id);
        $user->password = \Hash::make($userPutPassword['password']);
        $updated = $user->save();
        $message = $updated ? 'Contraseña Actualizada Correctamente' : 'Error al actualizar';
        return redirect()->route('user.index')->with('info', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        User::destroy($user->id);
        $message = 'Usuario Eliminado Correctamente';
        return redirect()->route('user.index')->with('info', $message);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\User  $id
     * @return \Illuminate\Http\Response
     */
    public function configure(String $id = null)
    {
        $citizen = Citizen::findOrFail($id);
        $statesSql = State::all();
        $rol = Rol::find(\Auth::user()->rol_id + 1);
        $name = $citizen->first_name;
        $rols = [];
        $states = [];

        if($citizen->last_name){
            $name = $citizen->first_name.' '.$citizen->last_name;
        }

        $user = array(
            'name' => $name,
            'email' => $citizen->email,
            'rol_id' => $citizen->rol_id,
            'citizen_id' => $citizen->id
        );

        $rols[$rol->id] = $rol->name;
        foreach($statesSql as $state){
            $states[$state->id] = $state->name;
        }
        return view('admin.user.create', compact(['rols', 'states', 'user']));
    }
}
