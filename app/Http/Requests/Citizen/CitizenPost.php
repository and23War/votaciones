<?php

namespace App\Http\Requests\Citizen;

use Illuminate\Foundation\Http\FormRequest;

class CitizenPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identification_card' => 'required|unique:citizens|max:10|min:7',
            'rol_id' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'identification_card.required' => 'Digitar Cédula',
            'identification_card.unique' => 'Cédula ya está registrada',
            'identification_card.max' => 'Cédula máximo 10 caracteres',
            'identification_card.min' => 'Cédula mínimo 7 caracteres',
            'rol_id.required'  => 'Selecccionar Rol',
        ];
    }
}
