<?php

namespace App\Http\Requests\Citizen;

use Illuminate\Foundation\Http\FormRequest;

class CitizenPut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

         return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {  

       return [
            
            'commune'       => 'required',
            'location'       => 'required',
           
        ];

    }


     public function messages()
    {
        return [
            'first_name.regex' => 'El Nombre solo debe de ser letras A-Z',
            'last_name.required' => 'Digitar Apellido',
            'department.required' => 'Seleccionar Departamento',
            'city.required' => 'Seleccionar Ciudad',
            'commune.required' => 'Seleccionar Comuna',
            'location.required' => 'Seleccionar Barrio de Residencia',
            'address.required' => 'Digitar Dirección',
            'phone.required' => 'Digitar Telefono',
            'email.required' => 'Digitar Email',


            'phone.integer' => 'El Telefono solo debe de ser numeros',
            'email.email' => 'Debe de ingresar una direccion valida de correo electronico',

            'first_name.max' => 'Nombre máximo 15 caracteres',
            'first_name.min' => 'Nombre mínimo 4 caracteres',
            'last_name.max' => 'Apellido máximo 15 caracteres',
            'last_name.min' => 'Apellido mínimo 4 caracteres',
            'address.max' => 'Direccion máximo 15 caracteres',
            'address.min' => 'Direccion mínimo 4 caracteres',
        ];
    }
}
