<?php

namespace App\Policies;

use App\User;
use App\Citizen;
use Illuminate\Auth\Access\HandlesAuthorization;

class CitizenPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any citizens.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {

    }

    /**
     * Determine whether the user can view the citizen.
     *
     * @param  \App\User  $user
     * @param  \App\Citizen  $citizen
     * @return mixed
     */
    public function view(User $user, Citizen $citizen)
    {
        return $user->citizen_id === $citizen->parent || $user->rol_id === 1 || $user->citizen_id === $citizen->id;
    }

    /**
     * Determine whether the user can create citizens.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

    }

    /**
     * Determine whether the user can update the citizen.
     *
     * @param  \App\User  $user
     * @param  \App\Citizen  $citizen
     * @return mixed
     */
    public function update(User $user, Citizen $citizen)
    {
        return $user->citizen_id === $citizen->parent || $user->rol_id === 1 || $user->citizen_id === $citizen->id;
    }

    /**
     * Determine whether the user can delete the citizen.
     *
     * @param  \App\User  $user
     * @param  \App\Citizen  $citizen
     * @return mixed
     */
    public function delete(User $user, Citizen $citizen)
    {
        return $user->citizen_id === $citizen->parent || $user->rol_id === 1;
    }

    /**
     * Determine whether the user can restore the citizen.
     *
     * @param  \App\User  $user
     * @param  \App\Citizen  $citizen
     * @return mixed
     */
    public function restore(User $user, Citizen $citizen)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the citizen.
     *
     * @param  \App\User  $user
     * @param  \App\Citizen  $citizen
     * @return mixed
     */
    public function forceDelete(User $user, Citizen $citizen)
    {
        return $user->citizen_id === $citizen->parent || $user->rol_id === 1;
    }
}
