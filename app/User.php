<?php

namespace App;

use App\Rol;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rol_id', 'state_id', 'citizen_id', 'parent',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the rol.
     */
    public function rol()
    {
        return $this->belongsTo('App\Rol', 'rol_id', 'id');
    }

    /**
     * Get the state.
     */
    public function state()
    {
        return $this->belongsTo('App\State', 'state_id', 'id');
    }

    /**
     * Get the rol.
     */
    public function citizen()
    {
        return $this->belongsTo('App\Citizen', 'citizen_id', 'id');
    }
}
