<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saveresponse extends Model
{
     protected $fillable = [
        'response'
    ];
}
