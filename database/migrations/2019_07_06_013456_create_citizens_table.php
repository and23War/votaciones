<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('identification_card')->unique();
            $table->string('email')->nullable();
            $table->string('department')->nullable();
            $table->string('city')->nullable();
            $table->integer('commune')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->unsignedBiginteger('rol_id');
            $table->unsignedBiginteger('parent')->nullable();

            $table->string('department_vp');
            $table->string('city_vp');
            $table->string('voting_post');
            $table->string('address_vp');
            $table->integer('table');
            $table->string('zone_vp')->nullable();
            $table->string('commune_vp')->nullable();

            $table->timestamps();
        });

        Schema::table('citizens', function (Blueprint $table) {
            $table->foreign('rol_id')->references('id')->on('rols');
            $table->foreign('parent')->references('id')->on('citizens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizens');
    }
}
