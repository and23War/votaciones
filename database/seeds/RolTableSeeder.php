<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Rol::create([
            'name' => 'Admin',
        ]);
        \App\Rol::create([
            'name' => 'Lider 1',
        ]);
        \App\Rol::create([
            'name' => 'Lider 2',
        ]);
        \App\Rol::create([
            'name' => 'Lider 3',
        ]);
        \App\Rol::create([
            'name' => 'Ciudadano',
        ]);
    }
}
