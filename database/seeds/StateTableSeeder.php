<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\State::create([
            'id' => 1,
            'name' => 'Activo',
        ]);
        \App\State::create([
            'id' => 2,
            'name' => 'Inactivo',
        ]);
    }
}
