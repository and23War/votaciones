$(document).ready(function(){
	

		var id_de=0;
		var comuna;


	$('#department').typeahead({
				
			hint: true,
			highlight: true,
			minLength: 1,
			
	
		source: function(query, process){
						
					$.ajax({
						url: 'http://localhost:8000/buscar/departamento/',
						headers : {'X-CSRF-TOKEN' : $('#token').val()},
						type: 'GET',
						dataType: 'json',
						data:{
							query:query,
						},
						success:function(data){
							
           					console.log(data);
           					 process(data);
								
							
							}
						});


					},
					// displayText: function(item){
					// 	$('#state').item;
					// },
					afterSelect : function(data){

						//console.log(data.name);
					
						id_de = data.id;

						
						return id_de;

					}

			});
// *******************************************************************************
	

			//console.log('id_departamento :'+id_de );

		$('#city').typeahead({
				
			hint: true,
			highlight: true,
			minLength: 1,
			
			  
		
		source: function(query, process){
						
			//alert('id_departamento :'+id_de );

					$.ajax({
						url: 'http://localhost:8000/buscar/ciudad',
						headers : {'X-CSRF-TOKEN' : $('#token').val()},
						type: 'GET',
						dataType: 'json',
						data:{
							query:query,
							id_de:id_de,
							
						},
						success:function(data){
								 	process(data);
									console.log(data);

									
								}
						});
					},
					// displayText: function(item){
					// 	$('#state').item;
					// },
					afterSelect : function(data){

						console.log(data.name);

					
					}

			});


		// *******************************************************************************
		//Barrios
	
	$("#commune").change(function(event){
					
				comuna = $('#commune').val();
				$('#location').empty();

				console.log(comuna);
					$.ajax({
						url: 'http://localhost:8000/buscar/barrio',
						headers : {'X-CSRF-TOKEN' : $('#token').val()},
						type: 'GET',
						dataType: 'json',
						data:{
							
							comuna:comuna,
						},
						success:function(data){
							
								for(i=0; i<data.length; i++){

                    			$("#location").append("<option value='"+data[i].name+"'>"+data[i].name+"</option>");
                         
								
							}
						}
			
		});
			
	});
});