$(document).ready(function(){

	var datatable = $("#data");
	var concejo 	= 	$("#concejo");

	var $pagination = $('#pagination');
	var totalRecords = 0;
			records = [];
			displayRecords = [];
	var recPerPage = 20;
	var	page = 1;
	var totalPages = 0;



	concejo.click(function(){
		
		// $("#concejo").attr('checked', true);  
		var token = $('#token').val();
		var route = 'http://localhost:8000/ajaxreport';
		var query = $('#concejo').val();
		 
	
		 $.ajax({
			 	type: 'POST',
			 	headers : {'X-CSRF-TOKEN' : token},
				url: route,
				dataType: 'json',
				data:{
					query : query,
					
				}, 			
				success: function(response){
					

					datatable.empty();
					datatable.fadeIn( "slow", function() {
							
					records = response;                  
        	totalRecords = records.length;
        	totalPages = Math.ceil(totalRecords / recPerPage);

          generate_table(records, totalRecords, totalPages);
						
						
					});
				},
				error: function(){
					console.log("A ocurrido un error");
				},
				

			});
		return true;
	});

	$('#alcaldia').click(function(){

		var token = $('#token').val();
		var route = 'http://localhost:8000/ajaxreport';
		var query = $('#alcaldia').val();
	

		 $.ajax({
			 	type: 'POST',
			 	headers : {'X-CSRF-TOKEN' : token},
				url: route,
				dataType: 'json',
				data:{
					query : query,
					
				}, 			
				success: function(response){
					
					datatable.empty();
					datatable.fadeIn( "slow", function() {
							
					records = response;                  
        	totalRecords = records.length;
        	totalPages = Math.ceil(totalRecords / recPerPage);

          generate_table(records, totalRecords, totalPages);
						
			
					});
				},
				error: function(){
					console.log("A ocurrido un error");
				},
				

			});
			return true;
	});

	$('#gobernacion').click(function(){

		var token = $('#token').val();
		var route = 'http://localhost:8000/ajaxreport';
		var query = $('#gobernacion').val();
	

		 $.ajax({
			 	type: 'POST',
			 	headers : {'X-CSRF-TOKEN' : token},
				url: route,
				dataType: 'json',
				data:{
					query : query,
					
				}, 			
				success: function(response){
					
					datatable.empty();
					datatable.fadeIn( "slow", function() {
							
					records = response;                  
        	totalRecords = records.length;
        	totalPages = Math.ceil(totalRecords / recPerPage);

          generate_table(records, totalRecords, totalPages);
						
			
					});
				},
				error: function(){
					console.log("A ocurrido un error");
				},
				

			});
			return true;
	});

	$('#todos').click(function(){

		alert('todos');
	});



	function generate_table(displayRecords) {
      
      var tr;

    	datatable.html('');
      
      	
      datatable.fadeIn( "slow", function() {
							
							for (var i = 0; i < displayRecords.length; i++) {
            tr = $('<tr/>');
            
            tr.append("<td>" + displayRecords[i].identification_card + "</td>");
            tr.append("<td>" + displayRecords[i].first_name +' '+displayRecords[i].last_name +"</td>");
						tr.append("<td>" + displayRecords[i].department_vp + "</td>");
						tr.append("<td>" + displayRecords[i].city_vp + "</td>");
						tr.append("<td>" + displayRecords[i].voting_post + "</td>");
           	
           	
            datatable.append(tr);
      }

						
						});

 	}
});