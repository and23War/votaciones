@extends('layouts.admin')
@section('content')

<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row p-t-20">
    <div class="col-12">
        <div class="page-header">
            <h2 class="pageheader-title"> Crear Referido </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Principal</a>
                        </li>

                        <li class="breadcrumb-item" aria-current="page"><a
                                href="{{route('citizen.index')}}">Referido</a></li>

                        <li class="breadcrumb-item active" aria-current="page">Crear</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12 col-sm-8">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        {!! Form::open(['method'=> 'POST', 'action'=>'Admin\CitizenController@store']) !!}
                        {{ csrf_field() }}
                            @include('admin.citizen.partials.form_create')
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
