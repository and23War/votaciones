@extends('layouts.admin')
@section('content')
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row p-t-20">
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
        <div class="page-header">
            <h2 class="pageheader-title"> Editar Referido </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{route('citizen.index')}}">Referidos</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Editar</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12 col-sm-8">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            {!! Form::model($citizen, ['route'=> ['citizen.update', $citizen->id ], 'method' => 'PUT', 'id' =>'FormEdit' ] )!!}
                            {{ csrf_field() }}
                                @include('admin.citizen.partials.form_edit')
                            {!! Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@section('scripts')

    


<script src="{{ asset('admin/js/query/citizenform.js') }}"></script>


@endsection

@endsection
