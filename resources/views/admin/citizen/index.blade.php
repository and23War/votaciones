@extends('layouts.admin')
@section('content')

<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row p-t-20">
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
        <div class="page-header">
            <h2 class="pageheader-title"> Referidos</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Principal</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Referidos</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->

<div class="row">
    <!-- ============================================================== -->
    <!-- data table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header d-flex">
                <h4 class="card-header-title">Registro de Referidos</h4>
                <div class="toolbar ml-auto">
                    <a href="{{ route('citizen.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus ml-auto w-auto"></i></a>
                </div>
            </div>
            @if (count($citizens) > 0)

            <div class="card-body">
                <div class="table-responsive">
                    <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="dt-buttons"><!--  <button
                                        class="btn btn-outline-light buttons-copy buttons-html5" tabindex="0"
                                        aria-controls="example" type="button"><span>Copy</span></button> -->
                                         <a
                                        class="btn btn-outline-light buttons-excel buttons-html5" tabindex="0"
                                        href="{{url('report')}}"><span>Excel</span></a> <button
                                        class="btn btn-outline-light buttons-pdf buttons-html5" tabindex="0"
                                        aria-controls="example" type="button"><span>PDF</span></button>
                                       <!--  <button
                                        class="btn btn-outline-light buttons-collection dropdown-toggle buttons-colvis"
                                        tabindex="0" aria-controls="example" type="button"
                                        aria-haspopup="true"><span>Column visibility</span></button> --> </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div id="example_filter" class="dataTables_filter">
                                    <label>
                                        Buscar: <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example" class="table table-striped table-bordered second dataTable"
                                    style="width: 100%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending"
                                                style="width: 148px;">Cedula</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending"
                                                style="width: 148px;">Nombre</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Position: activate to sort column ascending"
                                                style="width: 148px;">Rol</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Position: activate to sort column ascending"
                                                style="width: 180px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($citizens as $citizen)
                                            <tr role="row" class="odd">
                                                <td>{{$citizen->identification_card}}</td>
                                                <td class="sorting_1">{{$citizen->first_name}} {{$citizen->last_name}}</td>
                                                <td>
                                                {{$citizen->rol->name}}
                                                </td>
                                                <td>


                                                <div class="btn-group ml-auto">
                                                    
                                                    <a class="btn btn-sm btn-outline-light"  href="{{ route('citizen.show',  $citizen->id) }}">    <i class="far fas fa-eye"></i>
                                                    </a>
                                                  
                                                  @if (Auth::user()->rol_id < 4 

                                                     && Auth::user()->id !== $citizen->rol_id) 
                                                    <a class="btn btn-sm btn-outline-light"     href="{{ route('citizen.edit',  $citizen->id) }}">
                                                          <i class="far fas fa-edit"></i>
                                                        </a>
                                                       
                                                        {{Form::open(['method'  => 'DELETE', 'route' => ['citizen.destroy', $citizen->id]])}}
                                                            @php
                                                                $modal = [
                                                                    'id' => $citizen->id,
                                                                    'parent' => 'Citizen',
                                                                    'modalTitle' => "404, page not found",
                                                                    'modalBody' => "sorry, but the requested page does not exist :("
                                                                ];
                                                            @endphp
                                                        

                                                 {!!Form::open(['route'=> ['citizen.destroy', $citizen], 'method' => 'DELETE']) !!}
                             
                                                                                    
                                                      <button onClick="return confirm('Desea Eliminar el Registro ?')" class="btn btn-sm btn-outline-light">
                                                          <i class="fa fa-trash"></i></button>

                                                  {!!Form::close()!!}

                                                   
                                                      
                                                   
                                                        <a class="btn btn-sm btn-outline-light"
                                                            href="{{ url('user/configure',  $citizen->id) }}">
                                                            <i class="fas fa-cogs"></i>
                                                        </a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Cedula</th>
                                            <th rowspan="1" colspan="1">Nombre</th>
                                            <th rowspan="1" colspan="1">Rol</th>
                                            <th rowspan="1" colspan="1"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="citizens_info" role="status" aria-live="polite">Mostrando
                                    {{$citizens->firstItem()}} de {{$citizens->lastItem()}} total {{$citizens->total()}} Registros</div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="citizens_paginate">
                                    {{ $citizens->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
                @include('partials.none-records')
            @endif
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table  -->
    <!-- ============================================================== -->
</div>
@endsection
