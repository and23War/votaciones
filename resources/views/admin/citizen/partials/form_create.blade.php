<div class="card">
    <h5 class="card-header">Datos Referido</h5>
    <div class="card-body">
        <div class="form-group row {{ $errors->has('identification_card') ? ' has-error' : '' }}">
            {{ Form::label('identification_card', 'Cédula',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('identification_card', null, ['class' => 'form-control', 'id' => 'identification_card'])}}
            </div>
        </div>
            
        <div class="form-group row {{ $errors->has('eleccion') ? ' has-error' : '' }}">
            {{ Form::label('eleccion', 'Elección: ',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            
            <div class="col-12 col-sm-8 col-lg-6">
                

                <label class="custom-control custom-checkbox custom-control-inline">
                    <input value="1" name="asamblea" type="checkbox" class="custom-control-input"><span class="custom-control-label">Asamblea</span>
                </label>


                <label class="custom-control custom-checkbox custom-control-inline">
                    <input value="1" name="concejo" type="checkbox" class="custom-control-input"><span class="custom-control-label">Concejo</span>
                </label>

                <label class="custom-control custom-checkbox custom-control-inline">
                    <input value="1" name="alcaldia" type="checkbox" class="custom-control-input"><span class="custom-control-label">Alcaldia</span>
                </label>
                


                <label class="custom-control custom-checkbox custom-control-inline">
                    <input value="1" name="gobernacion" type="checkbox" class="custom-control-input"><span class="custom-control-label">Gobernacion</span>
                </label>

                <label class="custom-control custom-checkbox custom-control-inline">
                    <input value="1" name="todos" type="checkbox" class="custom-control-input"><span class="custom-control-label">Todos</span>
                </label>
            </div>
        </div>
     


        <div class="form-group row {{ $errors->has('rol_id') ? ' has-error' : '' }}">
            {{ Form::label('rol_id', 'Rol',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {!! Form::select('rol_id', [ null => 'Selecciona el rol']+ $rols, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-5">
                <button type="submit" class="btn btn-space btn-primary">Crear</button>
                <a class="btn btn-space btn-secondary" href="{{ route('citizen.index')}}">Cancelar</a>
            </div>
        </div>
    </div>
</div>
