

<div class="container card">
    <h5 class="card-header">Datos Referido</h5>

    <div class="card-body ">
 
    
        <div class="row">

          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('identification_card', 'Cédula',  ['class' => ''])}}
          </div>
            
            <div class="col-md-6 col-sm-12 col-lg-6">
                {{ Form::text('identification_card', null, ['class' => 'form-control', 'id' => 'identification_card', 'readonly'])}}
            </div>
        </div>


        <div class="row p-t-20">

          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('first_name', 'Nombre(s)',  ['class' => 'col-form-label text-sm-right', 'data-validation'=>'alphanumeric'])}}
          </div>
              
          <div class="col-md-3 col-sm-12 col-lg-3">
                {{ Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name'])}}
          </div>
        
          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right ">
             {{ Form::label('last_name', 'Apellido(s)',  ['class' => ' col-form-label text-sm-right'])}}
          </div>
              
          <div class="col-md-3 col-sm-12 col-lg-3">
             {{ Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name'])}}
          </div>

        </div>

      
        <div class="row p-t-20">

          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('department', 'Departamento Residencia',  ['class' => 'col-form-label text-sm-right'])}}
          </div>


          <div class="col-md-3 col-sm-12 col-lg-3">
                {{ Form::text('department', null, ['class' => 'form-control bs-autocomplete', 'id' => 'department', 'autocomplete'=>'off',   'data-provide' =>'typeahead'])}}

                
          </div>
          
          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('city', 'Ciudad Residencia',  ['class' => 'col-form-label text-sm-right'])}}
          </div>


          <div class="col-md-3 col-sm-12 col-lg-3">
                {{ Form::text('city', null, ['class' => 'form-control bs-autocomplete', 'id' => 'city', 'autocomplete'=>'off',   'data-provide' =>'typeahead'])}}
          </div>

        </div>

        <div class="row p-t-20">
           
          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
              {{ Form::label('commune', 'Comuna Residencia',  ['class' => ''])}}
            </div>
            
            <div class="col-3 col-sm-12 col-lg-3">
                {!! Form::select('commune', [ null => 'Seleccione la comuna']+ $commune, null, ['class' => 'form-control']) !!}
            </div>
            

             <div class="col-md-2 col-lg-2 text-sm-right">
              {{ Form::label('location', 'Barrio Residencia',  ['class' => 'text-sm-right'])}}


            </div>
            
            <div class="col-3 col-sm-12 col-lg-3">
               

              {!! Form::select('location', ['placeholder'=>'-Seleccione-'],null, ['id'=>'location', 'class'=>'form-control',])!!}    


            </div>
        
        </div>
      
        
        <div class="row p-t-20">
           
          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('address', 'Dirección Residencia',  ['class' => 'col-form-label text-sm-right'])}}
          </div>


          <div class="col-3 col-sm-12 col-lg-3">
                {{ Form::text('address', null, ['class' => 'form-control', 'id' => 'address'])}}
          </div>
        

          <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('phone', 'Teléfono Residencia',  ['class' => 'col-form-label text-sm-right', 'data-validation'=>'number'])}}
          </div>

          <div class="col-3 col-sm-12 col-lg-3">
              
              {{ Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone'])}}
          </div>
        </div>
        
        <div class="row p-t-20">

           <div class="col-md-2 col-lg-2 col-sm-12 text-sm-right">
            {{ Form::label('email', 'Correo Electrónico',  ['class' => 'col-form-label text-sm-right'])}}
          </div>
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email'])}}
            </div>
        </div>

        <div class="row p-t-20">

          <div class="col-2 col-sm-12 col-lg-2 text-sm-right">
              {{ Form::label('rol_id', 'Rol',  ['class' => 'col-form-label text-sm-right'])}}
          </div>


          <div class="col-3 col-sm-12 col-lg-3">
             
              {!! Form::select('rol_id', [ null => 'Selecciona el rol']+ $rols, null, ['class' => 'form-control']) !!}
          </div>
        </div>
    </div>
    <div class="card-body border-top">
        <h3>Datos de RN</h3>
        <div class="form-group row {{ $errors->has('department_vp') ? ' has-error' : '' }}">
            {{ Form::label('department_vp', 'Departamento Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('department_vp', null, ['class' => 'form-control', 'id' => 'department_vp', 'readonly'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('city_vp') ? ' has-error' : '' }}">
            {{ Form::label('city_vp', 'Ciudad Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('city_vp', null, ['class' => 'form-control', 'id' => 'city_vp', 'readonly'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('voting_post') ? ' has-error' : '' }}">
            {{ Form::label('voting_post', 'Puesto Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('voting_post', null, ['class' => 'form-control', 'id' => 'voting_post', 'readonly'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('address_vp') ? ' has-error' : '' }}">
            {{ Form::label('address_vp', 'Dirección Puesto Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('address_vp', null, ['class' => 'form-control', 'id' => 'address_vp', 'readonly'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('table') ? ' has-error' : '' }}">
            {{ Form::label('table', 'Mesa Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('table', null, ['class' => 'form-control', 'id' => 'table', 'readonly'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('zone_vp') ? ' has-error' : '' }}">
            {{ Form::label('zone_vp', 'Zona Puesto Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('zone_vp', null, ['class' => 'form-control', 'id' => 'zone_vp'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('commune_vp') ? ' has-error' : '' }}">
            {{ Form::label('commune_vp', 'Comuna Puesto Votación',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('commune_vp', null, ['class' => 'form-control', 'id' => 'commune_vp'])}}
            </div>
        </div>
        <div class="form-group row">
            <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-5">
                <button type="submit" class="btn btn-space btn-primary">Guardar</button>
                <a class="btn btn-space btn-secondary" href="{{ route('citizen.index')}}">Cancelar</a>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="token" value="{{ csrf_token() }}">

