@extends('layouts.admin')
@section('content')
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
        <div class="page-header">
            <h2 class="pageheader-title"> Ver Referido </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Dashboard</a>
                        </li>

                        <li class="breadcrumb-item" aria-current="page"><a href="{{route('citizen.index')}}"
                                class="breadcrumb-link">Referidos</a></li>

                        <li class="breadcrumb-item active" aria-current="page">Ver</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h4 class="mb-0">Datos</h4>
                        <div class="dropdown ml-auto">
                            <a class="toolbar" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-dots-vertical"></i>  </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Nombre(s): {{ $citizen->first_name }}</p>
                        <p>Apellido(s): {{ $citizen->last_name }}</p>
                        <p class="border-bottom pb-3">Cédula: {{ $citizen->identification_card }}</p>
                        <p>Correo Electrónico: {{ $citizen->email }}</p>
                        <p>Departamento Residencia:{{ $citizen->department }}</p>
                        <p>Ciudad Residencia: {{ $citizen->city }}</p>
                        <p>Comuna Residencia: {{ $citizen->commune }}</p>
                        <p>Dirección Residencia:{{ $citizen->address }}</p>
                        <p>Teléfono Residencia: {{ $citizen->phone }}</p>
                        <p>Teléfono Residencia: {{ $citizen->rol_id }}</p>
                        <p>Departamento Votación:{{ $citizen->department_vp }}</p>
                        <p>Ciudad Votación: {{ $citizen->city_vp }}</p>
                        <p>Puesto Votación: {{ $citizen->voting_post }}</p>
                        <p>Dirección Puesto Votación:{{ $citizen->address_vp }}</p>
                        <p>Mesa Votación: {{ $citizen->table }}</p>
                        <p>Zona Puesto Votación:{{ $citizen->zone_vp }}</p>
                        <p>Comuna Puesto Votación:{{ $citizen->commune_vp }}</p>
                       
                    </div>
                </div>

                <a class="btn btn-primary btn-md" href="{{ route('citizen.index') }}">Regresar</a>
            </div>
        </div>
    </div>
</div>

@endsection
