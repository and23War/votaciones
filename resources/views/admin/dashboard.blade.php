@extends('layouts.admin')
  @section('content')

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
              <h2 class="pageheader-title"> Principal </h2>
              <p class="pageheader-text"></p>
              <div class="page-breadcrumb">
                  <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Principal</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Administrador</li>
                      </ol>
                  </nav>
              </div>
          </div>
      </div>
    </div>



    <div class="row">

      <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
          <div class="card">
              <div class="card-body">
                  <div class="d-inline-block">
                      <h5 class="text-muted">Referidos Registrados</h5>
                      <h2 class="mb-0"></h2>
                  </div>
                  <div class="float-right icon-circle-medium  icon-box-lg  bg-info-light mt-1">
                      <i class="fa fa-users fa-fw fa-sm text-info"></i>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
          <div class="card">
              <div class="card-body">
                  <div class="d-inline-block">
                      <h5 class="text-muted">Grupos Registrados</h5>
                      <h2 class="mb-0"> </h2>
                  </div>
                  <div class="float-right icon-circle-medium  icon-box-lg  bg-primary-light mt-1">
                      <i class="fa fa-book fa-fw fa-sm text-primary"></i>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>


  @endsection
