@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title"> Reportes </h2>
                <p class="pageheader-text"></p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Reportes</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Administrador</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- ============================================================== -->
        <!-- data table rowgroup  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Generar Reporte</h5>
                    <p></p>
                </div>
                <div class="card-body">
                    @include('partials.alerts')
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Seleccione el tipo de reporte: </h3>
                                {!! Form::open(['method'=> 'POST', 'url'=>'reporte1' ]) !!}
                                {{ csrf_field() }}
                                <div class="form-group row {{ $errors->has('dato') ? ' has-error' : '' }}">
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <label class="custom-control custom-checkbox custom-control-inline">
                                            <input id="concejo" value="1" name="dato" type="checkbox" class="custom-control-input"><span class="custom-control-label">Concejo</span>
                                        </label>
                                        <label class="custom-control custom-checkbox custom-control-inline">
                                            <input id="alcaldia" value="2" name="dato" type="checkbox" class="custom-control-input"><span class="custom-control-label">Alcaldia</span>
                                        </label>
                                        <label class="custom-control custom-checkbox custom-control-inline">
                                            <input id="gobernacion" value="3" name="dato" type="checkbox" class="custom-control-input"><span class="custom-control-label">Gobernacion</span>
                                        </label>
                                        <label class="custom-control custom-checkbox custom-control-inline">
                                            <input id="todos" id="dato" value="4" name="dato" type="checkbox" class="custom-control-input"  ><span class="custom-control-label">Todos</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4 p-l-20">
                                        <button class="btn btn-md btn-primary">
                                            <i class="fa fa-fw fa-download"></i> Descargar
                                        </button>
                                    </div>
                                </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                    @if (count($citizens) > 0)
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered second dataTable" role="grid" aria-describedby="example_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" scope="col" rowspan="1" colspan="1" aria-sort="ascending">Cedula</th>
                                        <th class="sorting_asc" scope="col" rowspan="1" colspan="1" aria-sort="ascending">Nombre</th>
                                        <th class="sorting" scope="col" rowspan="1" colspan="1" >Departamento</th>
                                        <th class="sorting" scope="col" rowspan="1" colspan="1" >Ciudad</th>
                                        <th class="sorting" scope="col" rowspan="1" colspan="1" >Puesto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($citizens as $citizen)
                                        <tr>
                                            <td class="table-primary">{{$citizen->identification_card}}</td>
                                            <td class="sorting_1 table-primary">{{$citizen->first_name}} {{$citizen->last_name}}</td>
                                            <td class="table-primary">{{$citizen->department_vp}}</td>
                                            <td class="table-primary">{{$citizen->city_vp}}</td>
                                            <td class="table-primary">{{$citizen->voting_post}}</td>

                                        </tr>
                                        @foreach ($citizen->children as $citizen_c)
                                            <tr role="row">
                                                <td class="table-secondary">{{$citizen_c->identification_card}}</td>
                                                <td class="table-secondary">{{$citizen_c->first_name}} {{$citizen_c->last_name}}</td>
                                                <td class="table-secondary">{{$citizen_c->department_vp}}</td>
                                                <td class="table-secondary">{{$citizen_c->city_vp}}</td>
                                                <td class="table-secondary">{{$citizen_c->voting_post}}</td>
                                            </tr>
                                            @foreach ($citizen_c->children as $citizen_c_c)
                                            <tr role="row">
                                                <td class="table-light">{{$citizen_c_c->identification_card}}</td>
                                                <td class="table-light">{{$citizen_c_c->first_name}} {{$citizen_c_c->last_name}}</td>
                                                <td class="table-light">{{$citizen_c_c->department_vp}}</td>
                                                <td class="table-light">{{$citizen_c_c->city_vp}}</td>
                                                <td class="table-light">{{$citizen_c_c->voting_post}}</td>
                                            </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1" scope="col">Cedula</th>
                                        <th rowspan="1" colspan="1" scope="col">Nombre</th>
                                        <th rowspan="1" colspan="1" scope="col">Departamento</th>
                                        <th rowspan="1" colspan="1" scope="col">Ciudad</th>
                                        <th rowspan="1" colspan="1" scope="col">Puesto</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    @endif
                </div>

            </div>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
            <!-- ============================================================== -->
            <!-- end data table rowgroup  -->
            <!-- ============================================================== -->
        </div>
    </div>

    @section('scripts')
        <script src="{{ url('admin/js/jquery.twbsPagination.min.js') }}"></script>
        <script src="{{ url('admin/js/query/reporttable.js') }}"></script>
    @endsection
@endsection
