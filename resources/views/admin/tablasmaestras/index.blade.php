@extends('layouts.admin')

@section('content')



     <!-- ============================================================== -->
        <!--breadcrumb  -->
    <!-- ============================================================== -->

  <div class="row">  
   <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"> 
      <div class="page-header">
          <h2 class="pageheader-title">Tablas Maestras </h2>
          <p class="pageheader-text"></p>
          <div class="page-breadcrumb">
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Dashboard</a></li>
                      
                     
                       <li class="breadcrumb-item active" aria-current="page"> Tablas Maestras</li>

                       
                  </ol>
              </nav>
          </div>
      </div>
      
      
      @include('partials.alerts')
      
        <div id="load" class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-2">
                
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>

                       Cargando Solicitud...

                    </div>
                
            </div>
          </div>
        </div>

    </div>
  </div>
    <!-- ============================================================== -->
        <!--breadcrumb  -->
    <!-- ============================================================== -->
  <div class="row">
      <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"> 
      <div class="card">          
        <div class="card-header">
          <div class="float-left">
            <h5 class="">Listado de Tablas URL</h5>
          </div>

          <div class="float-right">
          <a href="{{url('dashboard')}}" class="btn btn-light btn-sm"> Regresar</a>
        </div>
          
         
        </div>
          
        <div class="card-body">
              <table class="table table-hover table-bordered " id="sampleTable">
                  <thead>
                      <tr>
                          <th>Id</th>
                          <th>Nombre</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>

                     
                        <tr>
                            <td>1</td>
                           <td>Cargar Archivos Excel - Cedulas - CSV </td>
                           <td>
                            <a href="{{url('upload')}}" class="btn btn-sm btn-info"><i class=" fas fa-angle-double-right "></i></a>
                          </td>
                        </tr>


                         <tr>
                            <td>2</td>
                           <td>Cargar Archivos Excel - Comunas - CSV </td>
                           <td>
                            <a href="{{url('uploadcomunas')}}" class="btn btn-sm btn-info"><i class=" fas fa-angle-double-right "></i></a>
                          </td>
                        </tr>
                      
                        <tr>
                            <td>3</td>
                           <td>Cargar Archivos Excel - Departamento - CSV </td>
                           <td>
                            <a href="{{url('uploaddept')}}" class="btn btn-sm btn-info"><i class=" fas fa-angle-double-right "></i></a>
                          </td>
                        </tr>


                          <tr>
                            <td>4</td>
                           <td>Cargar Archivos Excel - Ciudades - CSV </td>
                           <td>
                            <a href="{{url('uploadcity')}}" class="btn btn-sm btn-info"><i class=" fas fa-angle-double-right "></i></a>
                          </td>
                        </tr>

                      <tr>
                          <td>5</td>
                          <td>Ejecutar Consulta</td>
                          <td>
                            <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">

                            <a id="btnrun" class="btn btn-sm btn-info"><i class=" fas fa-angle-double-right "></i></a>

                          </td>
                      </tr>

                      
                   
                
                  </tbody>
              </table>
                   
          </div>
      </div>
    </div>

@section('scripts')

<script src="{{ url('admin/js/tablasmaestras/exploit.js') }}"></script>

@endsection  
@endsection