@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title"> Tablas Maestras </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Principal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Administrador</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <!-- ============================================================== -->
    <!-- data table rowgroup  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Cargar Excel con Ciudades </h5>
               
            </div>
            <div class="card-body">
                
                <div class="row">
                        
                            <form action="{{url('importcity')}}" method="post" enctype="multipart/form-data">
                                

                            {{ csrf_field() }}


                               <label>Cargar Archivo Excel</label> <br>
                                <input type="file" name="file"> <br><br>

                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <input class="btn btn-md btn-primary" type="submit" name="upload" value="upload">
                            </form>
                        </div>

            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table rowgroup  -->
    <!-- ============================================================== -->
</div>
</div>


@endsection
