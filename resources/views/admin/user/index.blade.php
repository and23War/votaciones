@extends('layouts.admin')
@section('content')

<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
        <div class="page-header">
            <h2 class="pageheader-title"> Usuarios </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Principal</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->

<div class="row">
    <!-- ============================================================== -->
    <!-- data table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header d-flex">
                <h4 class="card-header-title">Registro de Usuarios</h4>
            </div>
            @if (count($users) > 0)

            <div class="card-body">
                <div class="table-responsive">
                    <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="dt-buttons">
                                    
                                    <button class="btn btn-outline-light buttons-excel buttons-html5" tabindex="0"
                                        aria-controls="example" type="button"><span>Excel</span>
                                    </button>
                                    <button class="btn btn-outline-light buttons-pdf buttons-html5" tabindex="0"
                                        aria-controls="example" type="button"><span>PDF</span>
                                    </button>
                                    
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div id="example_filter" class="dataTables_filter">
                                    <label>
                                        Buscar: <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example" class="table table-striped table-bordered second dataTable"
                                    style="width: 100%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending"
                                                style="width: 148px;">Nombre</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending"
                                                style="width: 148px;">Correo electronico</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Position: activate to sort column ascending"
                                                style="width: 148px;">Rol</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Position: activate to sort column ascending"
                                                style="width: 148px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Position: activate to sort column ascending"
                                                style="width: 180px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($users as $user)
                                            <tr role="row" class="odd">
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->rol->name}}</td>
                                                <td>{{$user->state->name}}</td>
                                                <td>
                                                    <div class="btn-group ml-auto">

                                                       <a class="btn btn-sm btn-outline-light"
                                                            href="{{ route('user.show',  $user->id) }}">
                                                            <i class="far fas fa-eye"></i>
                                                        </a>


                                                        @if ($user->rol_id !== 1 && Auth::user()->id !== $user->id)

                                                        <a class="btn btn-sm btn-outline-light"
                                                            href="{{ route('user.edit',  $user->id) }}">
                                                            <i class="far fas fa-edit"></i>
                                                        </a>
                                                    
                                                            {{Form::open(['method'  => 'DELETE', 'route' => ['user.destroy', $user->id]])}}
                                                                @php
                                                                    $modal = [
                                                                        'id' => $user->id,
                                                                        'parent' => 'User',
                                                                        'modalTitle' => "404, Eliminar",
                                                                        'modalBody' => "Esta Seguro, Que desea eliminar el usuario "
                                                                    ];
                                                                @endphp
                                                                <a href="#" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#modal{{$modal['parent']}}{{$modal['id']}}">
                                                                    <i class="far fas fa-trash-alt"></i>
                                                                </a>
                                                                @include('partials.modal_delete', $modal)
                                                            {{Form::close()}}
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Nombre</th>
                                            <th rowspan="1" colspan="1">Correo electronico</th>
                                            <th rowspan="1" colspan="1">Rol</th>
                                            <th rowspan="1" colspan="1">Estado</th>
                                            <th rowspan="1" colspan="1"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="users_info" role="status" aria-live="polite">Mostrando 
                                    {{$users->firstItem()}} de {{$users->lastItem()}} Total {{$users->total()}} Registros</div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="users_paginate">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
                @include('partials.none-records')
            @endif
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table  -->
    <!-- ============================================================== -->
</div>
@endsection
