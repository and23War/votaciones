<div class="card">
    <h5 class="card-header">Datos Usuario</h5>
    <div class="card-body">
        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Nombre Completo',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                @if(empty($user['name']))
                    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name'])}}
                @else
                    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'readonly'])}}
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::label('email', 'Correo',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::label('password', 'Contraseña',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'id' => 'password'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::label('password_confirmation', 'Contraseña',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'id' => 'password_confirmation'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('rol_id') ? ' has-error' : '' }}">
            {{ Form::label('rol_id', 'Rol',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                @if(empty($user['rol_id']))
                    {!! Form::select('rol_id', [ null => 'Selecciona el rol']+ $rols, null, ['class' => 'form-control']) !!}
                @else
                    {!! Form::select('rol_id', $rols, null, ['class' => 'form-control', 'readonly']) !!}
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('state_id') ? ' has-error' : '' }}">
            {{ Form::label('state_id', 'Estado',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {!! Form::select('state_id', [ null => 'Selecciona el estado']+ $states, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        {{ Form::text('citizen_id', null, ['class' => 'form-control', 'id' => 'citizen_id', 'hidden'])}}
        <div class="form-group row">
            <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-5">
                <button type="submit" class="btn btn-space btn-primary">Crear</button>
                <a class="btn btn-space btn-secondary" href="{{ route('user.index')}}">Cancelar</a>
            </div>
        </div>
    </div>
</div>
