<div class="card">
    <h5 class="card-header">Password</h5>
    <div class="card-body">
        <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::label('password', 'Contraseña',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'id' => 'password'])}}
            </div>
        </div>
        <div class="form-group row {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::label('password_confirmation', 'Contraseña',  ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'id' => 'password_confirmation'])}}
            </div>
        </div>
        <div class="form-group row">
            <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-5">
                <button type="submit" class="btn btn-space btn-primary">Actualizar</button>
                <a class="btn btn-space btn-secondary" href="{{ route('user.index')}}">Cancelar</a>
            </div>
        </div>
    </div>
</div>
