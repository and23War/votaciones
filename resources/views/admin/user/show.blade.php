@extends('layouts.admin')
@section('content')
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
        <div class="page-header">
            <h2 class="pageheader-title"> Editar Usuario </h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{route('user.index')}}">Usuarios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Editar</li>
                    </ol>
                </nav>
            </div>
        </div>
        @include('partials.alerts')
    </div>
</div>
<!-- ============================================================== -->
<!--breadcrumb  -->
<!-- ============================================================== -->
<div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12 col-sm-8">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            {!! Form::model($user, ['route'=> ['user.update', $user['id'] ], 'method' => 'PUT' ] )!!}
                                {{ csrf_field() }}
                                @include('admin.user.partials.form_show')
                            {!! Form::close()!!}
                        </div>
                    </div>
                    <div class="row">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
