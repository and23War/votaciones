@extends('layouts.theme')

@section('content')

<div id="slides">
    <div class="bg-slides-t"></div>
    <div class="logos-alcaldia2">
      <img src="{{url('images/logos2/LOGOS-04.png')}}" width="115" height="95">
    </div>

    <div class="logos-uan">
      <img src="{{url('images/logos2/EAN-01.png')}}" width="115" height="95">
    </div>

    <div class="logo-vivelab2">
      <img src="{{url('images/Logo_Vivelab.png')}}" width="150" height="150">
    </div>
   
    <div class="newloginform">
      <div class="container">
        <div class="formregistro">
            <div class="forceColor"></div>
            
               <div class="topbar">
                    <div class="spanColor"></div>
                    <h1>Contactanos</h1>
                </div>
                
    <form class="form-horizontal" role="form" method="POST" action="{{ route('contact.store') }}">
                        {{ csrf_field() }}

        <div class="topbar">
                
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                   
                   
                    <div class="spanColor"></div>
                        <input id="name" type="text" class="input" name="name" placeholder="Nombre y Apellido" value="{{ old('name') }}" />

                         @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                 <div class="topbar">
                
                 <div class="form-group {{ $errors->has('telefono') ? ' has-error' : '' }}">
                   
                   
                    <div class="spanColor"></div>
                        <input id="telefono" type="text" class="input" name="telefono" placeholder="Telefono" value="{{ old('telefono') }}" />

                         @if ($errors->has('telefono'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

        <div class="topbar">
                
                 <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                   
                   
                    <div class="spanColor"></div>
                        <input id="email" type="email" class="input" name="email" placeholder="Correo electrónico" value="{{ old('email') }}" />

                         @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

             


           <div class="topbar">
                
                 <div class="form-group {{ $errors->has('texto') ? ' has-error' : '' }}">
                   
                   
                    <div class="spanColor"></div>
                        <input id="msj" type="textarea" class="input" name="msj" placeholder="Mensaje" value="{{ old('msj') }}" />

                         @if ($errors->has('msj'))
                            <span class="help-block">
                                <strong>{{ $errors->first('msj') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


  
            <button class="btn-submit" id="submit" >Enviar</button>

            
        </form>
        </div>
      </div>
    </div>
  

    <div class="slides-container">
      <img src="{{url('images/slide/pic2.jpg')}}" >
    </div>
    

</div>



@endsection
