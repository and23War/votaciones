@extends('layouts.theme2')

@section('content')

 <div class="bgmain"></div>
 
  <div class="main">
    <div class="row">
      <div class="col s12 m6">
        <div class="col s12 m8 offset-m4 ">
          <div class="center-align padd100">

            <img src="{{url('theme2/multimedia/img/LOGOS-03.png')}}" height="150" width="180">
            <img src="{{url('theme2/multimedia/img/EAN-01.png')}}" height="130" width="150">
            <img src="{{url('theme2/multimedia/img/LOGOS-04.png')}}" height="200" width="300">
          </div>
        </div>
      </div>
    
    <div class="col s12 m6 ">
      <div class="col s12 m8 offset-m2 ">
        <a href="" class="brillo center-align FontNeuropol FBold white-text"><span></span></a>
      </div>
    <div class="col s12 m8 offset-m2 ">
                            
      <div class="card-logins ">
        <div class="row">
         

           <form class="col s12 form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

            {{ csrf_field() }}

            <div class="row">
              <P class="center-align FontNeuropol FBold">Recuperar Contraseña</P>
              <hr class="hr1">
            </div>
            
            <div class="row">
              @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
            </div>

              <div class="row">
                <div class="input-field col s12">

                   <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

                    <input id="email" name="email" type="email" class="validate" value="{{ $email or old('email') }}" required>
                  
                    <label for="email">Correo Electronico</label>
                      
                      @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>


              
            
              
              <div class="row">
                <button class="waves-effect waves-light btn-large teal col s12" id="submit">Recuperar Contraseña</button>
              </div>
              
             
            </form>
          </div>
        </div>
      </div>
      
      
        </div>
      </div>
  </div> 
@endsection
