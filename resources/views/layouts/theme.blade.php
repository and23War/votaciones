<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Login</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{url('admin/css/bootstrap.min.css')}}">
        <link href="{{url('admin/fonts/circular-std/style.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{url('admin/css/style.css')}}">
        <link rel="stylesheet" href="{{url('admin/fonts/fontawesome/css/fontawesome-all.css')}}">
        <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
        }
        </style>
    </head>

    <body cz-shortcut-listen="true">

        @yield('content')

        <!-- Optional JavaScript -->
        <script src="{{url('admin/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{url('admin/js/bootstrap.bundle.js')}}"></script>


    </body>
</html>
