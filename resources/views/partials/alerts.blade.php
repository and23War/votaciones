 @if(session('info'))
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>

                        {{ session('info')}}

                    </div>
                
            </div>
        </div>
    </div>
@endif

@if(count($errors))
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endif

 @if(session('message_error'))
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>

                        {{ session('message_error')}}

                    </div>
                
            </div>
        </div>
    </div>
@endif