  <!-- Footer -->
     <footer class="aniview" data-av-animation="fadeInUp">
      <div class="footer ">
        <div class="container">
            <div class="row">
              <div class="col-md-5 col-md-offset-1 col-sm-12 paddingtop-bottom">
                  <h6 class="heading7">Vínculos de interés</h6>
                  <ul class="footer-ul">
                      <li><a href="http://www.bogota.gov.co/"> Alcadia Mayor de Bogota</a></li>
                      <li><a href="http://tic.bogota.gov.co/"> Alta Consejería Distrital de TIC</a></li>
                      <li><a href="http://universidadean.edu.co/seccion/inicio.html"> Universisdad EAN</a></li>
                      <li><a href="http://www.mintic.gov.co"> MinTIC</a></li>
                  </ul>
              </div>
              <div class="col-md-5 col-sm-12 paddingtop-bottom">
                  <h4 class="heading7">Comunícate con nosotros</h4>
                  <ul class="footer-ul">
                      <li><a> Dirección Cl. 71 # 9-84, Bogotá Universidad EAN - Av Chile </a></li>
                      <li><a> contacto@laboratoriodigitalbogota.com</a></li>
                     
                  </ul>
              </div>
              <div class="col-md-12 paddingtop-bottom">
                                
                <div class="logos-alcaldia-footer">
                  <img src="{{url('images/logos2/EAN-01.png')}}" width="120" height="100">

                  <img src="{{url('images/logos2/LOGOS-04.png')}}" width="190" height="120">                  

                  <img src="{{url('images/logos2/LOGOS-05.png')}}" width="330" height="100">
                      
                    
              </div>
                    
               </div>
            </div>
         </div>
      </div>
      <div class="designby">
        <div class="container">
          <span>Copyright 2017</span>
        </div>
      </div>        
    </footer>