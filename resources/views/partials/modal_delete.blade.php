<div class="modal fade" id="modal{{$parent}}{{$modal['id']}}" tabindex="-1" role="dialog" aria-labelledby="modalLabel{{$parent}}{{$modal['id']}}" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel{{$parent}}{{$modal['id']}}">{{$modalTitle}}</h5>
                <a href="#" class="close" data-dismiss="modal{{$parent}}{{$modal['id']}}" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="modal-body">
                <p>{{$modalBody}}</p>
            </div>
            <div class="modal-footer">
                 <button type="submit" class="btn btn-primary">Eliminar</button>

                <a href="#" class="btn btn-secondary" data-dismiss="modal">Cerrar</a>
               
            </div>
        </div>
    </div>
</div>
