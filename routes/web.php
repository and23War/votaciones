<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@getRegisterForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');

Route::get('user/configure/{id}', 'Admin\UserController@configure')->name('configure');
Route::put('user/updatePassword/{id}', 'Admin\UserController@updatePassword')->name('update.password');

Route::resource('user', 'Admin\UserController');
Route::resource('citizen', 'Admin\CitizenController');


Route::group(['middleware' => 'cors'], function(){

Route::get('buscar/departamento', 'Admin\TablasMaestrasController@Departament');
Route::get('buscar/ciudad', 'Admin\TablasMaestrasController@City');
Route::get('buscar/barrio', 'Admin\TablasMaestrasController@Location');


	});


// *************************************************
Route::get('tablasmaestras', 'Admin\TablasMaestrasController@index');
Route::get('upload', 'Admin\TablasMaestrasController@upload');
Route::post('import', 'Admin\ImportController@import');
Route::post('exploit', 'Admin\TablasMaestrasController@exploit');

//Cargar Comunas y Barrios
Route::get('uploadcomunas', 'Admin\TablasMaestrasController@uploadcomunas');
Route::post('importcomunas', 'Admin\ImportController@comunas');

//Cargar Departamentos
Route::get('uploaddept', 'Admin\TablasMaestrasController@uploaddept');
Route::post('importdept', 'Admin\ImportController@departamento');

//Cargar Ciudades
Route::get('uploadcity', 'Admin\TablasMaestrasController@uploadcity');

Route::post('importcity', 'Admin\ImportController@ciudad');


//Reportes
Route::get('report', 'Admin\ReportController@index')->name('report');
Route::post('reporte1', 'Admin\ReportController@reporte1');
Route::post('ajaxreport', 'Admin\ReportController@ajaxreport');
